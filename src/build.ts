import { ContentData, EditionArgs, EditionData, Formatter, PublicationArgs, PublicationData, Theme } from "@mfgames-writing/contracts";
import * as fs from "fs";
import * as path from "path";
import { loadConfig } from "./configs";
import { appendContents, loadContents, renderContents } from "./content";
import { loadModule } from "./plugins";
import Finder = require("fs-finder");
import mkdirp = require("mkdirp-promise");
import uuid = require("uuid");
import liquid = require("liquid-node");

export function runBuild(argv: any, packageJson: any, logger: any)
{
    // The entire build system is based on promises. We start with the publication
    // file loading and everything flows from there.
    let args = new PublicationArgs(logger, packageJson, argv);

    loadPublication(args, argv)
        .then(chooseEditions)
        .then((data: any) => { args.logger.info("Finished processing output"); });
}

/**
* Creates a promise that finds an appropriate `publication.ext` files (JSON or
* YAML) and loads it into memory. If one cannot be found, then this rejects the
* promise, otherwise it returns the resulting file.
*/
function loadPublication(args: PublicationArgs, argv: any): Promise<PublicationArgs>
{
    let promise = new Promise<PublicationArgs>((resolve, reject) =>
    {
        // If we have a file provided, use that.
        let publicationFile: string | undefined = undefined;

        if (argv.c)
        {
            // The user provided a -c or --config option. We need the absolute
            // path though to ensure everything works.
            publicationFile = path.resolve(argv.c);
        }
        else
        {
            // Find the publication file in the current working directory.
            let currentDir = path.resolve(process.cwd(), ".");
            args.logger.debug("currentDir", currentDir);

            // Attempt to find the file at the current directory. We look for common
            // file extensions such as "yaml", "yml", and "json".
            let files = Finder.in(currentDir).lookUp().findFiles("publication.<(ya?ml|json)>");
            args.logger.debug("publicationFiles", files);

            if (files.length == 0)
            {
                args.logger.error("Cannot find publication.{yaml,yml,json} from current directory.");
                return reject("Could not find publication configuration.");
            }

            // Set the configuration file.
            publicationFile = files[0];
        }

        // Load the configuration file.
        let config = loadConfig<PublicationData>(args.logger, publicationFile);

        if (!config)
        {
            throw new Error("Cannot load configuration");
        }

        args.rootDirectory = path.dirname(publicationFile);
        args.publicationFile = publicationFile;
        args.publication = config;

        // Ensure every content has a process data which is used to pass values.
        prepareContents(config.contents);

        // Load the `package.json` file if possible.
        let packageFilename = path.join(args.rootDirectory, "package.json");

        if (fs.existsSync(packageFilename))
        {
            // Load the package file.
            let packageBuffer = fs.readFileSync(packageFilename);
            let packageMetadata = JSON.parse(packageBuffer.toString());

            args.package = packageMetadata;
        }

        // Finish up processing this file.
        resolve(args);
    });

    // Return the resulting promise chain.
    return promise;
}

function chooseEditions(args: PublicationArgs): Promise<PublicationArgs>
{
    // Build up a list of each edition promise which handles processing of a
    // single edition.
    let promises = new Array<Promise<EditionArgs>>();

    for (let editionName in args.publication.editions)
    {
        // If we have arguments that list build, we want to filter that. If
        // there are no targets, then we include everything.
        if (args.argv._.length > 0)
        {
            if (args.argv._.indexOf(editionName) < 0)
            {
                continue;
            }
        }

        // Pull out the edition and extend and merge it as needed.
        let edition = args.publication.editions[editionName];
        edition = extendObjects(edition, args.publication);
        edition = mergeObjects(edition, args.publication.metadata);
        edition = mergeObjects(edition, args.package);
        edition = mergeObjects(edition, getDefaults());
        edition.rootDirectory = args.rootDirectory;
        edition.publicationFile = args.publicationFile;
        edition.editionName = editionName;

        // Create a promise to handle this one.
        let editionArgs = new EditionArgs(args, editionName, edition);

        promises.push(buildEdition(editionArgs));
    }

    // If we don't have any, we are going blow up.
    if (!promises.length)
    {
        args.logger.error("Cannot find at least one edition to build");
        return Promise.reject("Cannot find edition");
    }

    // If we have at least one edition, we return an "all" promise.
    return Promise.all(promises).then(() => args);
}

function buildEdition(args: EditionArgs): Promise<EditionArgs>
{
    // We need to resolve the output directory and filename.
    let promise: Promise<EditionArgs> = Promise.resolve(args);

    promise = promise.then((a: any) =>
    {
        // Resolve the output directory.
        let promise: Promise<any> = new Promise((resolve, reject) =>
        {
            let engine = new liquid.Engine();
            resolve(engine.parse(args.edition.outputDirectory));
        });
        promise = promise.then((t: any) =>
        {
            let parameters = {
                edition: args.edition
            };
            return t.render(parameters);
        });
        promise = promise.then((directory: any) =>
        {
            args.edition.outputDirectory = directory;
            return args;
        });

        // Resolve the filename.
        promise = promise.then((a: any) =>
        {
            let engine = new liquid.Engine();
            return engine.parse(args.edition.outputFilename);
        });
        promise = promise.then((t: any) =>
        {
            let parameters = {
                edition: args.edition
            };
            return t.render(parameters);
        });
        promise = promise.then((filename: any) =>
        {
            args.edition.outputFilename = filename;
            return args;
        });

        // Finish up with the proper output.
        return promise.then((a: any) => args);
    });

    // Format the edition.
    promise = promise.then((a: any) =>
    {
        // Pull out the edition data.
        args.logger.debug("edition", args.name, args.edition);

        // Load the formatter and theme.
        args.format = loadModule(args, args.edition.format)(args) as Formatter;
        args.theme = loadModule(args, args.edition.theme)(args) as Theme;

        if (!args.format)
        {
            args.logger.error("Could not load format plugin: " + args.edition.format);

            throw new Error();
        }

        if (!args.theme)
        {
            args.logger.error("Could not load theme plugin: " + args.edition.theme);

            throw new Error();
        }

        // Make sure the output directory exists. This creates a promise but
        // we don't care about the output, only that it was created.
        let promise = mkdirp(args.edition.outputDirectory);

        // Build up a chain of promises to format the book. We let the format
        // and theme initialize themselves (via `start`), process the content,
        // then finalize the theme and the format.
        //
        // We have to pass the "a" so we can reference the class method.
        promise = promise
            .then((v: any) => args)
            .then((a: any) => args.format.start(a))
            .then((a: any) => args.theme.start(a))
            .then((a: any) => appendContents(a))
            .then((a: any) => loadContents(a))
            .then((a: any) => renderContents(a))
            .then((a: any) => args.theme.finish(a))
            .then((a: any) => args.format.finish(a));
        return promise;
    });

    return promise;
}

function prepareContents(contents: ContentData[], parent: ContentData | undefined = undefined)
{
    if (contents)
    {
        for (let content of contents)
        {
            content.parent = parent;
            content.process = {};

            prepareContents(content.contents || [], content);
        }
    }
}

function pause()
{
    let milliseconds = 1000;
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++)
    {
        if ((new Date().getTime() - start) > milliseconds)
        {
            break;
        }
    }
}

function extendObjects(edition: EditionData, publication: PublicationData): EditionData
{
    // If we aren't extending anything, then we don't have to do anything.
    if (!edition.extends)
        return edition;

    // We are extending it, so pull it in.
    let mergeFrom = publication.editions[edition.extends];

    return this.extend(edition, mergeFrom) as EditionData;
}

function mergeObjects(object1: any, object2: any): any
{
    return { ...object2, ...object1 };
}

function getDefaults()
{
    return {
        lang: "en",
        uid: uuid.v4()
    };
}
