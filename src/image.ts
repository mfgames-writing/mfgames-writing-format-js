import { ContentArgs, EditionImagesData, FormatImageRequest } from "@mfgames-writing/contracts";
import * as crypto from "crypto";
import * as fs from "fs";
import jimp = require("jimp");
import * as path from "path";

export function processImages(content: ContentArgs): Promise<ContentArgs>
{
    // Create an initial promise to make chains easier.
    var promise: Promise<any> = Promise.resolve(content);

    // We need to go through every image and process each one.
    let html = content.text;

    html = html.replace(/<img ([^>]*?)src=(['"])(.+?)['"]/g, (match, prefix, quote, href) =>
    {
        // If we are already an absolute path, then skip it. Otherwise, make it
        // an absolute one. This simplifies the code for processing since we
        // don't hvae to worry about context.
        let imagePath = href;

        if (!path.isAbsolute(href))
        {
            imagePath = path.join(content.directory, imagePath);
        }

        // Figure out the information and operations we'll need to be processing
        // for this image.
        let imageData = getFormatImageRequest(content, href, imagePath);

        // Query the format to modify the data. This doesn't actually add the
        // image into the format but it does return a promise to do so.
        var formatImageResponse = content.format.addImage(content, imageData);

        if (formatImageResponse.include)
        {
            // Process the image as a process.
            promise = promise.then((c: any) => processImage(content, imageData));

            // Add the image into the format's promise.
            promise = promise.then((request: any) =>
                Promise.resolve(request).then((img: any) => formatImageResponse.callback(img)));
        }

        // Rebuild the substitution to avoid removing it.
        return `<img ${prefix}src=${quote}${formatImageResponse.href}${quote}`;
    });

    // Normalize the output, changing the buffer. There won't be any major
    // problem if the HTML changes or not.
    promise = promise.then((a: any) =>
    {
        content.text = html;
        content.buffer = new Buffer(html, "utf-8");
        return content;
    });

    // Return the resulting promise chain.
    return promise;
}

function getFormatImageRequest(content: ContentArgs, href: string, imagePath: string): FormatImageRequest
{
    // Pull out information from the image.
    let extension = path.extname(imagePath);
    let mime: any = jimp.MIME_JPEG;

    switch (extension)
    {
        case ".png":
            mime = jimp.MIME_PNG;
            break;
    }

    // Figure out alterations or changes that need to be made. We first build up
    // a hash of "sane" values and then allow for the controls to override that.
    let imagesControl: EditionImagesData = {
        scale: 1.00,
        grayscale: false,
        opaque: false
    };

    if (content.edition.images)
    {
        imagesControl = { ...imagesControl, ...content.edition.images };
    }

    // Covers are special because we don't want to scale the image.
    if (content.element === "cover")
    {
        imagesControl.scale = 1.00;
    }

    // Get the SHA-256 hash for the image, which we'll use as a key and to
    // prevent duplicates from being added. We prefix the ID with an "i"
    // because `epubcheck` requires internal IDs to not start with a number.
    // zip archive. This is done inline because we
    // aren't working with promises.
    let imageBuffer = fs.readFileSync(imagePath);
    let imageHash = crypto.createHash("sha256").update(imageBuffer).digest("hex");
    let imageId = `i${imageHash}`;

    // Return the new path.
    var results: FormatImageRequest = {
        href: href,
        imagePath: imagePath,
        buffer: imageBuffer,
        scale: imagesControl.scale,
        grayscale: imagesControl.grayscale,
        opaque: imagesControl.opaque,
        mime: mime,
        extension: extension
    };

    return results;
}

function processImage(content: ContentArgs, imageRequest: FormatImageRequest): Promise<FormatImageRequest>
{
    // Make a little noise.
    content.logger.debug(`Processing image: ${imageRequest.imagePath}`);

    // Start by reading in the image.
    var promise: Promise<any> = jimp.read(imageRequest.imagePath);

    promise = promise.then((image: any) =>
    {
        return image;
    });

    // Grayscaling the image.
    if (imageRequest.grayscale)
    {
        promise = promise.then((image: any) =>
        {
            return image.greyscale();
        });
    }

    // Making the image opaque.
    if (imageRequest.opaque)
    {
        promise = promise.then((image: any) =>
        {
            var width = image.bitmap.width;
            var height = image.bitmap.height;
            var color = 0xFFFFFF;
            var background = new jimp(width, height, color);
            return background.composite(image, 0, 0);
        });
    }

    // Scaling images.
    if (imageRequest.scale !== 1.0)
    {
        promise = promise.then((image: any) =>
        {
            return image.scale(imageRequest.scale);
        });
    }

    // After we're done processing, pull it out and return it.
    promise = promise.then((image: any) =>
    {
        return new Promise((resolve: any, reject: any) =>
        {
            let buffer = image.getBuffer(imageRequest.mime, (err: any, buffer: any) =>
            {
                if (err) reject(err);
                resolve(buffer);
            });
        });
    });
    promise = promise.then((buffer: any) =>
    {
        imageRequest.buffer = buffer;
        return imageRequest;
    });

    return promise;
}

/*
return jimp.read(data.buffer)
    .then((image: any) => {
        if (data.scale !== 1.00) {
            image = image.scale(data.scale);
                }
                return image;
            })
            .then((image: any) => {
                let buffer = image.getBuffer(data.mime, (err, buffer) => {
*/
