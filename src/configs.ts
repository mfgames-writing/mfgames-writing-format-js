import * as path from "path";
import * as fs from "fs";
import * as yaml from "js-yaml";

export function loadConfig<T>(logger: any, filename: string): T | undefined
{
    // We have the file, so attempt to load it into memory. This will either be
    // a YAML or a JSON file.
    let extension = path.extname(filename);
    logger.debug("publicationFile", filename + ", ext", extension);

    switch (extension)
    {
        case ".yaml":
        case ".yml":
            return loadYaml(filename) as T;
        case ".json":
            return loadJson(filename) as T;
    }

    // If we fell through to this point, we don't know how to parse that file
    // type.
    logger.error(`Cannot parse ${extension} file types`);

    return undefined;
}

function loadYaml(filename: string): any
{
    let text = loadText(filename);
    let data = yaml.safeLoad(text);
    return data;
}

function loadJson(filename: string): any
{
    return JSON.parse(loadText(filename));
}

function loadText(filename: string): string
{
    return fs.readFileSync(filename).toString();
}
