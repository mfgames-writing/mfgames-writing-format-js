import { EditionArgs } from "@mfgames-writing/contracts";
import * as fs from "fs";
import * as path from "path";

var loaded: { [index: string]: any } = {};

export function loadModule(args: EditionArgs, name: string): any
{
    // Report what we're doing. We want to avoid doing this a lot, so we only
    // show the message once.
    if (loaded[name])
    {
        return loaded[name];
    }

    args.logger.debug(`Loading module: ${name}`);

    // If the module starts with a "./" or a "/", then we first treat it as a
    // local path. We still have to figure out the path, though, because require
    // is based on this library's path, not the source file.
    let mod = undefined;

    if (name.match(/^\.?\//))
    {
        mod = loadModulePath(args, path.resolve(args.rootDirectory, name));
    }

    // Get the path to the standard NPM module.
    if (!mod)
    {
        mod = loadModulePath(args, name);
    }

    // Try the modules from the current working directory.
    let rootPath = path.join(process.cwd(), "node_modules");

    if (!mod)
    {
        mod = loadModulePath(args, path.join(rootPath, name));
    }

    // If we can't find it, then blow up.
    if (!mod)
    {
        args.logger.error(`Cannot find module: ${name}`);
        throw `Cannot find module: ${name}`;
    }

    // Return the resulting module.
    loaded[name] = mod;
    return mod;
}

function loadModulePath(args: EditionArgs, modulePath: string): any
{
    try
    {
        let results = require(modulePath);
        args.logger.debug(`Loaded ${modulePath}`);
        return results.default;
    } catch (exception)
    {
        return null;
    }
}
