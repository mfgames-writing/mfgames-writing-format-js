import * as readPackage from "read-pkg-up";
import * as tracer from "tracer";
import * as yargs from "yargs";
import { runBuild } from "./build";

// Set up TypeScript source map support.
require("source-map-support").install();

// Set up the build command.
var build_help = "Builds one or more editions of the book.";
function build_yargs(yargs: any)
{
    yargs
        .help("help")
        .alias("c", "config")
        .demand(0)
        .argv;
}

// Combine everything together to create a composite arguments which is used
// to parse the input and create the usage if required.
var argv = yargs
    .usage("mfgames-writing-format command")
    .help("help")
    .showHelpOnFail(true, "Specify --help for available options")
    .demand(1)
    .command("build", build_help, build_yargs)
    .argv;

// Set up logging based on the parameters.
let logger = tracer.colorConsole({
    format: "{{timestamp}} {{message}}",
    dateformat: "HH:MM:ss"
});

let json = readPackage.sync().pkg;
logger.info(`Starting ${json.name}@${json.version}`);

// Grab the first elements in the argv, that will be the virtual command we are
// running. Once we have that, pass it into the appropriate function.
var command = argv._.splice(0, 1)[0];

logger.debug(`Running command: ${command}`);

switch (command)
{
    case "build":
        runBuild(argv, json, logger);
        break;
}
