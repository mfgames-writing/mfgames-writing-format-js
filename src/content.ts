import * as path from "path";
import fs = require("mz/fs");
import * as marked from "marked";
import { EditionArgs, ContentArgs, ContentData } from "@mfgames-writing/contracts";
import incremental = require("incremental");
import liquid = require("liquid-node");
import yamlFrontMatter = require("yaml-front-matter");
import zpad = require("zpad");
import { processImages } from "./image";
import { loadModule } from "./plugins";

class AppendArgs
{
    public editionArgs: EditionArgs;
    public contentIndex: number = 0;

    constructor(editionArgs: EditionArgs)
    {
        this.editionArgs = editionArgs;
    }
}

/**
* Goes through the contents list of the publication and expands out any source
* patterns from the results, populating them into `args.contents`.
*/
export function appendContents(args: EditionArgs): Promise<EditionArgs>
{
    var appendArgs = new AppendArgs(args);
    let promise = Promise.resolve(appendArgs);

    if (args.publication.contents)
    {
        for (let content of args.publication.contents)
        {
            promise = promise.then(a => appendContent(appendArgs, content));
        }
    }

    return promise.then((aa: AppendArgs) => aa.editionArgs);
}

/**
* Appends the contents of a single item to `args.contents`.
*/
function appendContent(args: AppendArgs, originalContent: ContentData): Promise<AppendArgs>
{
    // We need to work with a copy of the content.
    let content = { ...originalContent };

    // See if this content has been filtered out.
    if (content.exclude)
    {
        if (content.exclude.editions &&
            content.exclude.editions.indexOf(args.editionArgs.name) >= 0)
        {
            args.editionArgs.logger.debug("excluding edition", args.editionArgs.name);
            return Promise.resolve(args);
        }
    }

    // Otherwise, create the contents and process theme. We need to sequence the
    // content from the beginning to give them a unique identifier.
    return new Promise<AppendArgs>((resolve, reject) =>
    {
        // Figure out the directory we'll be processing for this.
        let directory = content.directory
            ? path.join(args.editionArgs.rootDirectory, content.directory)
            : args.editionArgs.rootDirectory;

        // Figure out if we have a parent.
        let parentArgs = content.parent && "ContentArgs" in content.parent.process
            ? content.parent.process["ContentArgs"]
            : undefined;

        // If we don't have a source, then we intend to generate the results.
        let patternMatch = content.source
            ? content.source.match(/^\/(.+)\/$/)
            : undefined;

        if (patternMatch)
        {
            // If this starts and stops with a "/", it is a regex. If we have that, then
            // we scan the directory to get the files. Otherwise, we just use the source
            // as a straight filename.

            // Go through all the files in the directory.
            let sourceRegex = new RegExp(patternMatch[1]);
            let promise = Promise.resolve(args);

            for (let file of fs.readdirSync(directory))
            {
                if (sourceRegex.test(file))
                {
                    // Create a copy of the content with the pattern populated.
                    let filename = file;
                    let patternContent: ContentData = {
                        directory: directory,
                        element: content.element,
                        number: content["number"],
                        source: filename,
                        liquid: content.liquid,
                        parent: content.parent,
                        start: content.start,
                        page: content.page,
                        pipeline: content.pipeline,
                        process: {}
                    };

                    // Add the promise to handle this into the pipeline.
                    let contentArgs = new ContentArgs(
                        args.editionArgs,
                        patternContent,
                        ++args.contentIndex);
                    contentArgs.parent = parentArgs;
                    args.editionArgs.contents.push(contentArgs);

                    // Only the first one in a match is going to be the first one.
                    // Likewise, we reset the forced page counter.
                    content.start = false;
                    content.page = undefined;

                    // If we have a number, then increment it.
                    if (content.number)
                    {
                        content.number = incremental(content.number);
                    }
                }
            }
        }
        else
        {
            let contentArgs = new ContentArgs(
                args.editionArgs,
                content,
                ++args.contentIndex);
            contentArgs.parent = parentArgs;
            args.editionArgs.contents.push(contentArgs);
            content.process["ContentArgs"] = contentArgs;
        }

        // Finish up the promise.
        resolve(args);
    });
}

/**
* Loads information about the contents including parsing the files into memory
* and loading metadata into the contents.
*/
export function loadContents(args: EditionArgs): Promise<EditionArgs>
{
    // Loading can be done in any order. While normally we don't want to load
    // everything into memory, typically these books are small enough to fit
    // in the space so we are going to just do it.
    let promises: Promise<ContentArgs>[] = [];

    for (let content of args.contents)
    {
        promises.push(loadContent(content));
    }

    // Combine everything together at the end.
    return Promise.all(promises).then(p => args);
}

/**
* Creates a promise that constructs a loading pipeline for the various content
* elements used for the edition.
*/
function loadContent(args: ContentArgs): Promise<ContentArgs>
{
    // Start with a basic promise, this makes it easier to build the chain.
    let promise = Promise.resolve(args);

    // If we don't have a source, then we have nothing to load.
    if (!args.contentData.source)
    {
        return promise
            .then(loadContentGeneration);
    }

    // Figure out the directory and filename for this content.
    let ext = args.extension;

    // If this is an image, then just replace it with an HTML to include it.
    if (ext === ".jpg" || ext === ".png")
    {
        promise = promise.then(loadContentImage);
    }

    // If this is Markdown, then we need to render it.
    if (ext === ".md" || ext === ".markdown" ||
        ext === ".htm" || ext === ".html")
    {
        promise = promise.then(loadContentText);
    }

    // Return the resulting promise.
    return promise;
}

/**
* Loads a direct image by wrapping a HTML file around it and then faking the
* results.
*/
function loadContentImage(content: ContentArgs): Promise<ContentArgs>
{
    return new Promise<ContentArgs>((resolve, reject) =>
    {
        // Figure out the alt tag for the image. This is required for EPUB.
        let isCover = content.element === "cover";
        let alt = isCover
            ? content.edition.title + " cover"
            : "";

        // Set up the content elements with the faked HTML.
        let filename = content.filename;

        content.source = content.source + ".html";
        content.metadata = {
            title: isCover ? "Cover" : "Image",
            element: content.element,
            directory: content.directory
        };
        content.text = `<p class="cover">
            <img src="${filename}" alt="${alt}" />
        </p>`;

        // Resolve the promise.
        content.logger.info(`Wrapping image into ${content.source}`);
        resolve(content);
    });
}

/**
* Loads the text and YAML header from a given text file.
*/
function loadContentText(content: ContentArgs): Promise<ContentArgs>
{
    // Start the promise chain with reading files into memory.
    var promise: Promise<any> = fs.readFile(content.filename);

    // Pull out the contents into a text buffer.
    promise = promise.then(buffer =>
    {
        let metadata = yamlFrontMatter.loadFront(buffer, "_");
        let text = metadata["_"];
        delete metadata["_"];

        content.metadata = { ...content.metadata, ...metadata };
        content.text = text;
        content.buffer = buffer;

        return content;
    });

    // If we have a pipeline, then we want to funnel the content through each
    // one in turn.
    if (content.pipeline)
    {
        for (let pipeline of content.pipeline)
        {
            let moduleName = pipeline.module;
            let pipelineModule = loadModule(content.editionArgs, moduleName);
            let pipelineInstance = pipelineModule(pipeline);

            promise = promise.then(c => pipelineInstance.process(c));
        }
    }

    // Return the resulting promise chain.
    return promise;
}

function loadContentGeneration(content: ContentArgs): Promise<ContentArgs>
{
    return new Promise<ContentArgs>((resolve, reject) =>
    {
        switch (content.element)
        {
            case "toc":
                var params: any = content.contentData;
                var title = params.title ? params.title : "Table of Contents";
                content.metadata.title = title;
                break;
            default:
                throw Error(`Cannot generate content for element type ${content.element}.`);
        }
        resolve(content);
    });
}

/**
* Renders the various content for inclusion with the format.
*/
export function renderContents(args: EditionArgs): Promise<EditionArgs>
{
    // Renders each of the contents and prepares them to be added to the format.
    // This has to be in order because of the NCX and OPF generation.
    let promise: Promise<any> = Promise.resolve(args);

    for (let content of args.contents)
    {
        promise = promise.then(e => renderContent(content));
    }

    // Combine everything together at the end.
    return promise.then(c => args);
}

/**
* Renders a single content and prepares it for including into the format.
*/
function renderContent(args: ContentArgs): Promise<ContentArgs>
{
    // Start with a basic promise, this makes it easier to build the chain.
    let promise = Promise.resolve(args);

    // If we don't have a source, then we have special rules for formatting.
    if (!args.contentData.source)
    {
        // Parse the input and create the basic HTML out of it.
        promise = promise.then(renderContentGeneration);
        promise = promise.then(a => args.theme.renderHtml(a));

        // The above rendering doesn't include things like <html> or style. We
        // only add these if we are wrapping each file individually. For single
        // document files, such as Word documents or some PDF, we don't do this.
        let settings = args.format.getSettings();

        if (settings.wrapIndividualFiles)
        {
            promise = promise.then(a => args.theme.renderLayout(a));
        }

        // Go through the images and process them. Once we do, we hand the
        // information over to the formatter for inclusion.
        promise = promise.then(a => processImages(args));

        // Give the HTML to the formatter to do whatever it needs to do.
        promise = promise.then(a => args.format.addHtml(a));
        return promise;
    }

    // Figure out the directory and filename for this content.
    let ext = args.extension;

    // If we are identified to have a liquid template inside, we need to
    // resolve that template first and rebuild the text.
    if (args.contentData.liquid)
    {
        promise = promise.then(renderContentLiquid);
    }

    // If this is Markdown, then we need to render it.
    if (ext === ".md" || ext === ".markdown")
    {
        promise = promise.then(renderContentMarkdown);
    }

    // If this is Markdown or HTML, then render it via the theme.
    if (ext === ".md" || ext === ".markdown" ||
        ext === ".htm" || ext === ".html")
    {
        // Figure out if we need to wrap the individual file.
        var settings = args.format.getSettings();

        promise = promise.then(a => args.theme.renderHtml(a));

        if (settings.wrapIndividualFiles)
        {
            promise = promise.then(a => args.theme.renderLayout(a));
        }

        // Go through the images and process them. Once we do, we hand the
        // information over to the formatter for inclusion.
        promise = promise.then(a => processImages(args));

        // Add the rendered HTML to the list.
        promise = promise.then(a => args.format.addHtml(a));
    }

    // Return the resulting promise.
    return promise;
}

/**
* Renders the contents of the file as a Liquid template and resolves it.
*/
function renderContentLiquid(content: ContentArgs): Promise<ContentArgs>
{
    return new Promise<ContentArgs>((resolve, reject) =>
    {
        // Report what we are doing.
        content.logger.info("Rendering Liquid contents");

        // Create a template from the text of the contents. The final one is
        // actually a promise.
        let engine = new liquid.Engine();
        let template = content.text;
        let promise = engine.parse(template)
            .then((t: any) =>
            {
                const parameters = {
                    content: content.metadata,
                    edition: content.edition,
                    theme: this
                };
                parameters.content.depth = content.depth;

                return t.render(parameters);
            })
            .then((rendered: any) =>
            {
                content.text = rendered;
                return content;
            });
        resolve(promise);
    });
}

/**
* Renders a Markdown file as HTML.
*/
function renderContentMarkdown(content: ContentArgs): Promise<ContentArgs>
{
    return new Promise<ContentArgs>((resolve, reject) =>
    {
        // Set up `marked` with common settings. We wrap the <img/> renderer
        // to include a trailing tag. We also give the formatter a chance
        // to manipulate the results before passing it in.
        let renderer = new marked.Renderer();
        let renderer2 = new marked.Renderer();

        renderer.hr = () =>
        {
            return content.theme.renderRule
                ? content.theme.renderRule(content)
                : renderer2.hr();
        };
        renderer.strong = (text) =>
        {
            return content.theme.renderStrong
                ? content.theme.renderStrong(content, text)
                : renderer2.strong(text);
        };
        renderer.em = (text) =>
        {
            return content.theme.renderEmphasis
                ? content.theme.renderEmphasis(content, text)
                : renderer2.em(text);
        };
        renderer.codespan = (text) =>
        {
            return content.theme.renderCodeSpan
                ? content.theme.renderCodeSpan(content, text)
                : renderer2.codespan(text);
        };
        renderer.del = (text) =>
        {
            return content.theme.renderDelete
                ? content.theme.renderDelete(content, text)
                : renderer2.del(text);
        };
        renderer.link = (href, title, text) =>
        {
            return content.theme.renderLink
                ? content.theme.renderLink(content, href, title, text)
                : renderer2.link(href, title, text);
        };
        renderer.blockquote = (quote) =>
        {
            return content.theme.renderBlockquote
                ? content.theme.renderBlockquote(content, quote)
                : renderer2.blockquote(quote);
        };
        renderer.image = (href, title, text) =>
        {
            // We allow attributes in the href of the tag.
            let results = parseAttributes(href);

            // Call the original img implementation but close the tag to
            // make it proper XHTML.
            let html = renderer2.image(results.text, title, text);
            html = html.replace(/>$/, results.attributes + " />");

            // See if we have any other post processing.
            if (content.theme.processImage)
            {
                html = content.theme.processImage(content, html);
            }

            return html;
        };
        renderer.paragraph = (text) =>
        {
            // Figure out if we have any attributes associated with this
            // paragraph. If we do, then parse them out.
            var results = parseAttributes(text);

            // Let the overriding theme alter the paragraph to allow for
            // letterines or special formatting.
            let para = `<p${results.attributes}>${results.text}</p>\n`;

            if (content.theme.processParagraph)
            {
                para = content.theme.processParagraph(content, para);
            }

            // Write out the results.
            return para;
        };

        marked.setOptions({
            renderer: renderer,
            gfm: false,
            tables: true,
            breaks: false,
            pedantic: false,
            sanitize: false,
            smartLists: true,
            smartypants: true
        });

        // Process the markdown content.
        let html = marked(content.text);

        // For some reason, certain authors like me insist on having a em-dash
        // at the end of a quote to represent interruptions. However, the
        // typography park of Marked doesn't really handle quotes properly in
        // that case.
        html = html.replace(/—“/g, "—”");

        // We also have a problem with italics at the end of quotes.
        html = html.replace(/<\/em>“/g, "</em>”");

        // Markdown has an interesting quirk that a "break" is actually a
        // backslash followed by a space. However, trailing spaces are difficult
        // to work with when you use EditorConfig to remove trailing them
        // because no sane person can actually see them, so we just treat a
        // backslash at the end of the line as that explicit break.
        html = html.replace(/\\\n/g, "<br/>\n");

        // We want to tag paragraphs that only have a single image in them.
        html = html.replace(
            /<p><img([^>]+?)\s+\/><\/p>/g,
            (r, i) =>
            {
                // If we don't have a class, add it.
                if (i.indexOf("class=") < 0)
                {
                    i += " class=\"\"";
                }

                //. Add the single image into the class.
                i = i.replace(/class=(.)/, "class=$1single-image ");

                // Return the results.
                return `<p class="single-image"><img${i} /></p>`;
            });

        // Set the content and resolve this promise.
        content.text = html;

        resolve(content);
    });
}

/**
* Renders content based on the element type.
*/
function renderContentGeneration(content: ContentArgs): Promise<ContentArgs>
{
    // Figure out the type.
    switch (content.element)
    {
        case "toc":
            return content.theme.renderTableOfContents(content);
        default:
            throw Error(`Cannot generate content for element type ${content.element}.`);
    }
}

/**
* The components pulled out after the call to parseAttributes.
*/
interface ParseAttributesResults
{
    text: string;
    attributes: string;
    classes: string[];
    id: string | undefined;
}

/**
* Parses a string to pull out an attribute string ({:...}) from the given
* text and return the parsed results.
*
* @param {string} text The text to parse.
* @param {string?} quote The quote character to use, defaults to '"'.
* @returns {ParseAttributesResults} The split out results.
*/
function parseAttributes(text: string, quote: string = "\""): ParseAttributesResults
{
    let classes: string[] = [];
    let id = undefined;
    let removed = text.replace(/\s*\{:(.*?)\}/, (match, attributes) =>
    {
        // Split the attributes on spaces.
        let attrs = attributes.trim().split(/\s+/);

        for (let attr of attrs)
        {
            // Grab the starting key and the rest of it.
            let m = attr.match(/^([.#])(.*)$/);

            if (m)
            {
                switch (m[1])
                {
                    case ".":
                        classes.push(m[2]);
                        break;
                    case "#":
                        id = m[2];
                        break;
                }
            }
        }

        // Just remove the attributes.
        return "";
    });

    // Format the attributes.
    let c = "";

    if (classes.length > 0)
    {
        c += " class=" + quote + classes.join(" ") + quote;
    }

    if (id)
    {
        c += " id=" + quote + id + quote;
    }

    // Build up the results.
    return {
        text: removed,
        attributes: c,
        classes: classes,
        id: id
    };
}
