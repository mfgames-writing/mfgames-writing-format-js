# [2.0.0](https://gitlab.com/mfgames-writing/mfgames-writing-format-js/compare/v1.1.1...v2.0.0) (2018-08-18)


### Bug Fixes

* **markdown:** do not santize input since we trust input ([354f0e3](https://gitlab.com/mfgames-writing/mfgames-writing-format-js/commit/354f0e3))


### BREAKING CHANGES

* **markdown:** HTML is no longer sanatized

## [1.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-format-js/compare/v1.1.0...v1.1.1) (2018-08-11)


### Bug Fixes

* switching to package management ([9f6d460](https://gitlab.com/mfgames-writing/mfgames-writing-format-js/commit/9f6d460))
